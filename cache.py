from m5.objects import Cache

class L1Cache(Cache):
    assoc=2
    tag_latency=2
    data_latency=2
    response_latency=2
    mshrs=4
    tgts_per_mshr=20

    def connect_CPU(self,cpu):
        raise NotImplementedError

    def connect_BUS(self, bus):
        self.mem_side = bus.slave


class L1ICache(L1Cache):
    size='16kB'

    def connect_CPU(self,cpu):
        self.cpu_side=cpu.icache_port


class L1DCache(L1Cache):
    size='64kB'
    
    def connect_CPU(self, cpu):
        self.cpu_side=cpu.dcache_port

class L2Cache(Cache):
    size='256kB'
    assoc=8
    tag_latency=20
    data_latency=20
    response_latency=20
    mshrs=20
    tgts_per_mshr=12

    def connect_CPUSideBus(self,bus):
        self.cpu_side=bus.master

    def connect_MemSideBus(self, bus):
        self.mem_side=bus.slave



