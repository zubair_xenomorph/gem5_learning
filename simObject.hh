#ifndef __LEARNING_SIMOBJECT_HH__
#define __LEARNING_SIMOBJECT_HH__

#include "params/simObject.hh"
#include "sim/sim_object.hh"

class simObject : public SimObject
{
public:
  simObject(simObjectParams *params);
};
#endif



