import m5
from m5.objects import *
from cache import *

system = System()

system.clk_domain = SrcClockDomain()
system.clk_domain.clock = '1GHz'
system.clk_domain.voltage_domain = VoltageDomain()

#system.cpu.icache = L1ICache()
#system.cpu.dcache = L1DCache()



system.mem_mode = 'timing'

system.mem_ranges = [AddrRange('512MB')]

system.cpu = TimingSimpleCPU()

system.cpu.icache=L1ICache()
system.cpu.dcache=L1DCache()
system.membus = SystemXBar()

system.l2bus = L2XBar()


system.cpu.icache.connect_CPU(system.cpu)
system.cpu.dcache.connect_CPU(system.cpu)

#system.cpu.icache_port = system.membus.slave
#system.cpu.dcache_port = system.membus.slave

system.cpu.icache.connect_BUS(system.l2bus)
system.cpu.dcache.connect_BUS(system.l2bus)

system.l2cache=L2Cache()
system.l2cache.connect_CPUSideBus(system.l2bus)
system.l2cache.connect_MemSideBus(system.membus)

system.cpu.createInterruptController()

system.cpu.interrupts[0].pio=system.membus.master
system.cpu.interrupts[0].int_master=system.membus.slave
system.cpu.interrupts[0].int_slave=system.membus.master

system.system_port = system.membus.slave

system.mem_ctrl = DDR3_1600_8x8()
system.mem_ctrl.range=system.mem_ranges[0]
system.mem_ctrl.port=system.membus.master

process = Process()
process.cmd = ['tests/test-progs/hello/bin/x86/linux/hello']
system.cpu.workload = process
system.cpu.createThreads()

root = Root(full_system=False, system = system)

m5.instantiate()

print ("Beginning")
m5.simulate()

print ("Done")

